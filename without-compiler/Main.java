import java.io.IOException;

public class Main {
   public static void main(String[] args) {
       try {
	   Runtime.getRuntime().exec("javac --version");
	   System.out.println("javac found");
	   System.exit(1);
       } catch (IOException e) {
	   // This is what should happen
       }
   }
}
